﻿using System;

namespace LoopControlDemo
{
    class Program
    {/// <summary>
    /// this code do something
    /// </summary>
    /// <param name="args"></param>
        static void Main(string[] args)
        {
            /* for(int i=1;i<101;i++)
             {
                 if (((i % 2 == 0) && (i % 3 == 0)) && (i % 5 == 0))
                     Console.WriteLine(i);

             }

             for (int i = 111; i < 990; i++)
             {
                 int k = 0;
                 for(int a=1;a<=i;a++)
                 {
                     if(i%a==0)
                     { k++; }
                 }
                 if(k==2)
                 {
                     Console.WriteLine(i +"is prime");

                 }*/

            A1 a1 = new A1();
            a1.grading();
            A2 a2 = new A2();
              a2.eligible();
            A3 a3 = new A3();
             a3.sumOfmultiple();
            A4 a4 = new A4();
            a4.disp();
            Console.ReadKey();
         }


               
        }
    

   
    class A1
    {
        private float marks;
        private string name;
        private string div;
        public void grading()
        {
            Console.WriteLine("Assignment 1:");
            Console.WriteLine("Enter Name:");
            name=Console.ReadLine();
            Console.WriteLine("Enter Marks:");
            marks = Convert.ToSingle(Console.ReadLine());

            if (marks <= 40 && marks < 50)
                div = "3rd class";
            if (marks <= 50 && marks < 60)
                div = "2nd class";
            if (marks <= 60 && marks < 66)
                div = "1st class";
            if (marks <= 66)
                div = "Distinction";
            Console.WriteLine("you scored"+marks+" marks and  got : "+div);

        }
    }

 class A2
    {
        private float phy, che, maths, total,avg;
        public void eligible()
        {
            Console.WriteLine("Assignment 2:");
            Console.WriteLine("Enter Marks in physics :");
            phy= Convert.ToSingle(Console.ReadLine());
            Console.WriteLine("Enter Marks in chemistry :");
            che = Convert.ToSingle(Console.ReadLine());
            Console.WriteLine("Enter Marks in maths :");
            maths = Convert.ToSingle(Console.ReadLine());
            total = phy + che + maths;
            avg = total / 3;
            if(((maths>=65&&phy>=55)&&che>=50)&&total>=180)
            {
                Console.WriteLine("you are Eligible to take admission and your average is "+avg);
            }
            else
            {
                Console.WriteLine("you are NOTEligible to take admission and your average is " + avg);
            }
        }
    }
 
    class A3
    {
        private int x,y,firstSumOfMultiple, secondSumOfMultiple,res;
        ushort k;
       public void sumOfmultiple()
        {
            Console.WriteLine("Assignment 3:");
            Console.WriteLine("Enter value of x:");
            x =Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter value of y:");
            y =Convert.ToInt32(Console.ReadLine());
            
    
            k = 1; firstSumOfMultiple = 0;
               for(;k<11;k++)
            {
                firstSumOfMultiple = firstSumOfMultiple + (k*x);

            }


            k = 1; secondSumOfMultiple = 0;

            for (; k < 11; k++)
            {
                secondSumOfMultiple = secondSumOfMultiple + (k * y);

            }




            Console.WriteLine("firstSumofMultiple " + firstSumOfMultiple);
            Console.WriteLine("secondSumofMultiple " + secondSumOfMultiple);
            if (firstSumOfMultiple > secondSumOfMultiple)
            {
                res = firstSumOfMultiple / secondSumOfMultiple;
                Console.WriteLine("firstSumOfMultiple / secondSumOfMultiple: " + res);
            }
            else
            {
                res = secondSumOfMultiple / firstSumOfMultiple;
                Console.WriteLine("secondSumOfMultiple / firstSumOfMultiple: " + res);
            }
            
        }
    }
    class A4
    { private int a;
      public  void disp()
        {
            Console.WriteLine("Assignment 4:");
            //Console.WriteLine("Enter value of b:");
            //b= Convert.ToInt32(Console.ReadLine());
            for (int i=-5;i<=5;i++)
            {
                a =Convert.ToInt32(Math.Pow(i,2))+ (2 * i) + 1;
                Console.WriteLine(" value of a:" + a);
            }
           
        }
    }

}

