﻿using MVCASYNCDEMO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace MVCASYNCDEMO
{
    public class CustomerService
    {
        public async Task<List<Customer>> GetCustomersAsync()
        {
            List<Customer> list = new List<Customer>();
            for(int i=0;i<10;i++)
            {
                Customer cust = new Customer();
                cust.custid = i;
                list.Add(cust);


            }
               

            return list;
        }
    }
}