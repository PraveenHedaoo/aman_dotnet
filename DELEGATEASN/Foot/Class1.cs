﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foot
{
    public class FOOT
    {
        public delegate double Conversion(double feet);
        public Conversion conv;

        public  double FeetToInch(double feet)
        {
          return conv(feet);
        }
    }
}
