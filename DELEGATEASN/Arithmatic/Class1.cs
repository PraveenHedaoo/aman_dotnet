﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arithmatic
{
    public class AritOp
    {
        public delegate int arithmetic(int n);
        public arithmetic ar;

        public int calc(int n)
        {
            return ar(n);
        }
    }
}
