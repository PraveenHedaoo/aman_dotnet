﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Foot;
using Arithmatic;
namespace DELEGATEASN
{
   
    delegate void CustomDel(string s);
    class Program
    {
        static int x = 0;
        static void Main(string[] args)
        {
            FOOT f = new FOOT();//as1
            f.conv = feettoinch;
            double inchval=f.FeetToInch(6);
            Console.WriteLine(inchval);

            AritOp ar = new AritOp();//as2
            ar.ar += Add;
            ar.ar += mul;
            ar.ar += sub;
            ar.calc(5);


            CustomDel del1 = Good;//as3
            CustomDel del2 = Morning;
            del1 += del2;
            del1("hi");

            del1 = del1 - del2;
            del1("hello");


            CelciusToFarhenite();//as4
            DigtoNum();//as5

            Console.Read();

        }
        static int Add(int n)
        {
             x = 10;
            x += n;
                Console.WriteLine("Sum " +x);
            return x;
        }

        static int sub(int n)
        {
            x = 10;
            x -= n;
            Console.WriteLine("Sub " + x);
            return x;
        }

        static int mul(int n)
        {
            x = 10;
            x *= n;
            Console.WriteLine("mul " + x);
            return x;
        }
        static double feettoinch(double ft)
        {
            return ft * 12;
        }

        static void Good(string s)//as3
        {
            System.Console.WriteLine("  Good, {0}!", s);
        }

        static void Morning(string s)//as3
        {
            System.Console.WriteLine("  Morning, {0}!", s);
        }

        static void CelciusToFarhenite()//as4
        {
            double celsius;
            Console.Write("Enter Fahrenheit temperature : ");
            double fahrenheit = Convert.ToDouble(Console.ReadLine());
            celsius = (fahrenheit - 32) * 5 / 9;
            Console.WriteLine("The converted Celsius temperature is" + celsius);
        }

        static void DigtoNum()
        {
            string[] digits = { "zero", "one", "two",
                        "three", "four", "five",
                        "six", "seven", "eight",
                        "nine" };

            Console.WriteLine("Enter Number :");
            string str=(Console.ReadLine());
            string res = "";
          //  Console.WriteLine(str.Length);

            int num = Convert.ToInt32(str),tmp1=num;
            int[] ar = new int[str.Length];

           for(int i= str.Length-1;i>=0;i--)
            {
                ar[i] = tmp1 % 10;
                tmp1 = tmp1 / 10;
                //Console.WriteLine(ar[i]);
            }
            
           for(int i=0;i< str.Length;i++)
            { switch (ar[i])
                {
                    case 0:
                        {
                            res += "zero ";
                            break;
                        }
                    case 1:
                        {
                            res += "one ";
                            break;
                        }
                    case 2:
                        res += "two ";
                        break;
                    case 3:
                        {
                            res += "three ";
                            break;
                        }
                    case 4:
                        {
                            res += "four ";
                            break;
                        }
                    case 5:
                        {
                            res += "five ";
                            break;
                        }
                    case 6:
                        {
                            res += "six ";
                            break;
                        }
                    case 7:
                        {
                            res += "seven ";
                            break;
                        }
                    case 8:
                        {
                            res += "eight ";
                            break;
                        }
                    case 9:
                        {
                            res += "nine ";
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            }

            Console.WriteLine(res);


        }

    }
}
