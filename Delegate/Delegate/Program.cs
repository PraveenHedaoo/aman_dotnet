using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FactorialClassn;
namespace Delegate
{
    class Program
    {
        static void Main(string[] args)
        {

            FactorialClass fc = new FactorialClass();
            //fc.wheretosend = DisplayFactorial;//registering 
            //fc.Calculate(5);


             fc.wheretosend += DisplayFactorial;//multi subscribe
             fc.wheretosend += DisplayFactorialWhenGreaterThan100;//multi subscribe

             fc.Calculate(5);
            //fc.wheretosend(10);was also valid without calculate bein  called but when we made wheretosend event it become in valid we can only subscribe 

            fc.wheretosend -= DisplayFactorial;//multi unsubscribe
            fc.wheretosend -= DisplayFactorialWhenGreaterThan100;//multi unsubscribe

            //int res = fc.Calculate(5);
            //Console.WriteLine(res);
            Console.Read();

        }

        static void DisplayFactorial(int number)
        {
            Console.WriteLine("Callback " + number);
        }

        static void DisplayFactorialWhenGreaterThan100(int num)
        {
            if (num > 100)
                Console.WriteLine("big value");
            else
                Console.WriteLine("small value");
        }
    }
}
