﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oops
{
    public static class Ex1
    {
        public static void Checkexp(this Contractor ct)
        {
            if (ct.exp < 5)
                Console.WriteLine(ct.name + "has experience <5 yrs");
            else
                Console.WriteLine(ct.name + "has experience >5 yrs");
        }

        public static void Checkexp(this FTE ct)
        {
            if (ct.exp < 5)
                Console.WriteLine(ct.name + "has experience <5 yrs");
            else
                Console.WriteLine(ct.name + "has experience >5 yrs");
        }
    }
}
