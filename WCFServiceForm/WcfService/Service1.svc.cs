﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

using WcfService.ServiceReference;

namespace WcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        public Student GetData(int value)
        {
            StudentClient client = new StudentClient();
            var obj=client.DisplayById(value);
            
            Student student = new Student();
             student.id = obj.id;
             student.name =obj.name;
             student.email = obj.email;
            return student;
        }

       public string Delete1(int value)
        {
            StudentClient client = new StudentClient();
            var obj = client.Delete(value);

            return obj;
        }
        public void insertp(int id,string name,string email)
        {
            StudentClient client = new StudentClient();
            client.AddStudent(id, name, email);
           

        }

        public void updatep(int id, string name, string email)
        {
            StudentClient client = new StudentClient();
            client.UpdateStudent(id, name, email);


        }


        //public CompositeType GetDataUsingDataContract(CompositeType composite)
        //{
        //    if (composite == null)
        //    {
        //        throw new ArgumentNullException("composite");
        //    }
        //    if (composite.BoolValue)
        //    {
        //        composite.StringValue += "Suffix";
        //    }
        //    return composite;
        //}
    }
}
