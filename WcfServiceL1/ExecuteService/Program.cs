﻿using ExecuteService.ServiceReference1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExecuteService
{
    class Program
    {
        static void Main(string[] args)
        {
            EmployeClient client = new EmployeClient();
            ServiceReference1.ClassicEmp emp = new ServiceReference1.ClassicEmp() { name = "Aman", empno = 1234, grade = "ab" };
            //ClassicEmp emp = new ClassicEmp() { name = "Aman", empno = 1234,grade='a' };
            var x=client.disp(emp);
            Console.WriteLine(x);
            Console.ReadKey();

        }
    }
}
