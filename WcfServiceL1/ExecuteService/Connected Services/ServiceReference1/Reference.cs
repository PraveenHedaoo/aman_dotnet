﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ExecuteService.ServiceReference1 {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Employee", Namespace="http://schemas.datacontract.org/2004/07/WcfServiceL1")]
    [System.SerializableAttribute()]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(ExecuteService.ServiceReference1.ClassicEmp))]
    public partial class Employee : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int empnoField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string nameField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int empno {
            get {
                return this.empnoField;
            }
            set {
                if ((this.empnoField.Equals(value) != true)) {
                    this.empnoField = value;
                    this.RaisePropertyChanged("empno");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string name {
            get {
                return this.nameField;
            }
            set {
                if ((object.ReferenceEquals(this.nameField, value) != true)) {
                    this.nameField = value;
                    this.RaisePropertyChanged("name");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ClassicEmp", Namespace="http://schemas.datacontract.org/2004/07/WcfServiceL1")]
    [System.SerializableAttribute()]
    public partial class ClassicEmp : ExecuteService.ServiceReference1.Employee {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string gradeField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string grade {
            get {
                return this.gradeField;
            }
            set {
                if ((object.ReferenceEquals(this.gradeField, value) != true)) {
                    this.gradeField = value;
                    this.RaisePropertyChanged("grade");
                }
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceReference1.IEmploye")]
    public interface IEmploye {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IEmploye/disp", ReplyAction="http://tempuri.org/IEmploye/dispResponse")]
        string disp(ExecuteService.ServiceReference1.ClassicEmp e);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IEmploye/disp", ReplyAction="http://tempuri.org/IEmploye/dispResponse")]
        System.Threading.Tasks.Task<string> dispAsync(ExecuteService.ServiceReference1.ClassicEmp e);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IEmployeChannel : ExecuteService.ServiceReference1.IEmploye, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class EmployeClient : System.ServiceModel.ClientBase<ExecuteService.ServiceReference1.IEmploye>, ExecuteService.ServiceReference1.IEmploye {
        
        public EmployeClient() {
        }
        
        public EmployeClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public EmployeClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public EmployeClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public EmployeClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public string disp(ExecuteService.ServiceReference1.ClassicEmp e) {
            return base.Channel.disp(e);
        }
        
        public System.Threading.Tasks.Task<string> dispAsync(ExecuteService.ServiceReference1.ClassicEmp e) {
            return base.Channel.dispAsync(e);
        }
    }
}
