﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WcfServiceL1
{
    [KnownType(typeof(ClassicEmp))]
    [DataContract]
    public class Employee
    {
        [DataMember]

        public string name { get; set; }

        [DataMember]
        public int empno { get; set; }
    }
    public class ClassicEmp:Employee
    {
        public string grade;
    }
}
