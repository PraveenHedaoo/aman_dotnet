﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperatorandCast
{
    class Customer
    {
        public string Name { get; set; }
        public Order[] Orders { get; set; }
    }
}
