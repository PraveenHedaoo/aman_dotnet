﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using pluralB;

namespace ACM.BLTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Class1 c1 = new Class1();
            c1.name = "Aman";
            //c1.cash = 0;
            Assert.IsNull(c1.cash);
            Assert.IsNotNull(Class1.val);
            Assert.AreEqual(c1.name, "Aman");
        }
    }
}
