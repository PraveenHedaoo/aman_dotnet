﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading;

namespace MessagetypeWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    [ServiceBehavior(InstanceContextMode =InstanceContextMode.Single,ConcurrencyMode =ConcurrencyMode.Single)]
    public class Service1 : IService1
    { public int i;
        int intcounter = 0;
        public string RequestReplyOperation()
        {
            DateTime dtStart = DateTime.Now;
            Thread.Sleep(5000);
            DateTime dtEnd = DateTime.Now;
            return dtEnd.Subtract(dtStart).Seconds.ToString() + "second processing time";

            //return string.Format("You entered: {0}", value);
        }

        public void OnewayOperation()
        {
            Thread.Sleep(5000);
            return;
        }
        public int Increment()
        {
            intcounter++;
            return intcounter;
        }

        public string call(string cname)
        {
            return("client name:" + cname + "Instance :" + "Thread :" + Thread.CurrentThread.ManagedThreadId.ToString() + "Time" + DateTime.Now.ToString());
             Thread.Sleep(5000);
        }
      
    }
}
