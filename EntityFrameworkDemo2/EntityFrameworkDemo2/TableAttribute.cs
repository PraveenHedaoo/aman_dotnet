﻿using System;

namespace EntityFrameworkDemo2
{
    internal class TableAttribute : Attribute
    {
        private string v;

        public TableAttribute(string v)
        {
            this.v = v;
        }
    }
    [Table]
    class Sample
    { }

}