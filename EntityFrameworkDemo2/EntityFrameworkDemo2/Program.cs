﻿using EntityFrameworkDemo2.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkDemo2
{
    class Program
    {
        static IRepository<Department> repository;
        static void Main(string[] args)
        {
            repository = new Repository<Department>();
            var listOfDepartments = GetAll(repository);

            foreach (var item in listOfDepartments)
            {
                // Do Iteration on List OF Departments 
                Console.WriteLine("The name of Department is " + item.Name);
            }
        }

        private static IEnumerable<Department> GetAll(IRepository<Department> repository)
        {
            return repository.GetAll();
        }
    }
}
