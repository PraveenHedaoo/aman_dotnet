﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace EntityFrameworkDemo2
{
    public class SampledDBInitializer : DropCreateDatabaseAlways<SampleDbContext>
    {
        private List<Department> departments;

        protected override void Seed(SampleDbContext context)
        {
            departments = new List<Department>
            {
                new Department(){Location="Location1",Name="Name1"},
                new Department(){Location="Location2",Name="Name2"}
            };

            context.Departments.AddRange(departments);
            base.Seed(context);
        }
    }
}
