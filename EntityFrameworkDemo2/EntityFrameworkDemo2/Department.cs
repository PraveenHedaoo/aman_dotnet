﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkDemo2
{
    public class Department
    {
        [key]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
    }
}
