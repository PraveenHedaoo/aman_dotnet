﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkDemo2
{
    public class SampleDbContext : DbContext
    {
        public SampleDbContext()
        {
            Database.SetInitializer(new SampledDBInitializer());
        }
        public DbSet<Department> Departments { get; set; }
    }
}
