﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkDemo2.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private SampleDbContext sampleDbContext;
        private DbSet<T> dbSet;

        public Repository()
        {
            sampleDbContext = new SampleDbContext();
            dbSet = sampleDbContext.Set<T>();
        }

        //Get Operation
        public IEnumerable<T> GetAll()
        {
            return dbSet.ToList();
        }



        public void Save()
        {
            sampleDbContext.SaveChanges();
        }
    }
}
