﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCDEMO1.customValidators
{
    public class MyCustomValidator:ValidationAttribute
    {
        private readonly int _maximumWords;
        public MyCustomValidator(int maximumWords)
        {
            _maximumWords = maximumWords;
        }

       protected override ValidationResult IsValid(object value,ValidationContext validationContext)
        {
            if (value == null)
                return ValidationResult.Success;
            var userinputValue = Convert.ToString(value);
            if(userinputValue.Split(' ').Length<=_maximumWords)
                return ValidationResult.Success;
            var errorMessage = FormatErrorMessage((validationContext.DisplayName));
            return new ValidationResult(errorMessage);

        }
            
    }
}