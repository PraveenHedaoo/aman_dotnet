﻿using MVCDEMO1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;


namespace MVCDEMO1.Controllers
{
    public class StudentController : Controller
    {
        StudentEntities ds = new StudentEntities();
        // GET: Student
        //public ActionResult Index()
        //{
        //    return View();
        //}

        public ActionResult GetStudentsStronglyCoupled()
        {
            List<StudentViewModel> studentList = new List<StudentViewModel>
            {
                new StudentViewModel{id="1",FirstName="Firstname1",LastName="LastName1",Title="title1",age=21 },new StudentViewModel{id="2",FirstName="Firstname2",LastName="LastName2",Title="title2",age=22 }
            };
            return View(studentList);
        }

        public ActionResult GetStudentsLooselyCoupled()
        {
            List<StudentViewModel> studentList = new List<StudentViewModel>
            {
                new StudentViewModel{id="1",FirstName="Firstname1",LastName="LastName1",Title="title1",age=21 },new StudentViewModel{id="2",FirstName="Firstname2",LastName="LastName2",Title="title2",age=22 }
            };
            return View(studentList);
        }

        public ActionResult GetStudentById(int id)
        {
            StudentViewModel student = new StudentViewModel { id = "1", FirstName = "Firstname1", LastName = "LastName1", Title = "title1", age = 21 };


            return View(student);   
        }

        public ActionResult create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult create(StudentViewModel student)
        {
            if (ModelState.IsValid)
            {
                //db operation to iinsert
                return RedirectToAction(nameof(GetStudentsStronglyCoupled));
            }
            return null;
        }


        [HttpPost]
        public ActionResult GetStudentDetails(StudentViewModel student)
        {
            if(ModelState.IsValid)
            {
                return RedirectToAction(nameof(GetStudentsStronglyCoupled));
            }
            return null;
        }

        public ActionResult InsertData(StuModel st)
        {
            if(ModelState.IsValid)
            {
                ds.stud(st.id,st.FirstName,st.LastName,st.Title,st.age);
                return View();
            }
            return View("Error");
        }
   


    }
}